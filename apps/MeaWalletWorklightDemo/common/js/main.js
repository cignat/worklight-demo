var busy, readMsgs = [];

function onLoadMarketplaceSuccess(response) {
	try {
		//alert(JSON.stringify(response.invocationResult.Url, null, 4));
		var widgetList = response.invocationResult.Widgets;
		var widgetLength = widgetList.length;
		if (widgetLength <= 0) {
			$('#marketplaceContent').html("No new cards available for install.");
		} else {
			$('#marketplaceContent').html("");
			var widgetWidth = getThumbnailWidth();
			for (var index = 0; index < widgetLength; ++index) {
			    var widget = response.invocationResult.Widgets[index];
			    var widgetContent = "<a href='javascript:openWidget(\"" + widget.Id + "\")'>" +
			    		"<table style='display:inline-block;margin:5px; padding:5px;" +
			    		"width:" + widgetWidth + "px;float:left;border:1px solid #ddd;" +
	    				"-box-shadow: rgba(0,0,0,0.2) 1px 1px 1px;-webkit-box-shadow: rgba(0,0,0,0.2) " +
	    				"1px 1px 1px;-moz-box-shadow: rgba(0,0,0,0.2) 1px 1px 1px; '>" + 
			    		"<tr style='height:60px'><td align='center' width='50%'><img width='32' src='" + widget.Image +
		    			"' /></td><td width='50%' align='center'>" +
			    		widget.Title + "</td></tr></table></a>";
			    $('#marketplaceContent').append(widgetContent);
			}
		}
		enableTab('marketplaceBtn');
		busy.hide();
	} catch (e) {
		busy.hide();
		ShowErrorMessage(e);
	}
}

function onLoadWalletContentSuccess(response) {
	try {
		//alert(JSON.stringify(response.invocationResult.Url, null, 4));
		var widgetList = response.invocationResult.Widgets;
		var widgetLength = widgetList.length;
		if (widgetLength <= 0) {
			$('#walletContent').html('No cards in your wallet.<br /><a href="javascript:loadMarketplace()">Go to marketplace</a>');
		} else {
			$('#walletContent').html("");
			var widgetWidth = getThumbnailWidth();
			for (var index = 0; index < widgetLength; ++index) {
			    var widget = response.invocationResult.Widgets[index];
			    var widgetContent = "<a href='javascript:openWidget(\"" + widget.Id + "\")'>" +
			    		"<table style='display:inline-block;margin:5px; padding:5px;" +
			    		"width:" + widgetWidth + "px;float:left;border:1px solid #ddd;" +
	    				"-box-shadow: rgba(0,0,0,0.2) 1px 1px 1px;-webkit-box-shadow: rgba(0,0,0,0.2) " +
	    				"1px 1px 1px;-moz-box-shadow: rgba(0,0,0,0.2) 1px 1px 1px; '>" + 
			    		"<tr style='height:60px'><td align='center' width='50%'><img width='32' src='" + widget.Image +
		    			"' /></td><td width='50%' align='center'>" +
			    		widget.Title + "</td></tr></table></a>";
			    $('#walletContent').append(widgetContent);
			}
		}
		enableTab('walletBtn');
		busy.hide();
	} catch (e) {
		busy.hide();
		ShowErrorMessage(e);
	}
}

function onLoadInboxSuccess(response) {
	try {
//		alert(JSON.stringify(response.invocationResult, null, 4));
		displayItems(response.invocationResult.Items);
		enableTab('inboxBtn');
		$('#descriptionTab').hide();
		busy.hide();
	} catch (e) {
		busy.hide();
		ShowErrorMessage(e);
	}
}

function onOpenWidgetSuccess(response) {
	try {
		//alert(JSON.stringify(response.invocationResult, null, 4));
		var widgetContent = response.invocationResult.Content;
		$('#widget').html("");
	    $('#widget').append(widgetContent);
	    if ($('#walletBtn').attr('class') == 'optbutton selected') $('#widget').append("<a href='javascript:deleteWidget(\"" + response.invocationResult.Id + "\")' id='installed' style='position:absolute;left:20px;bottom:10px'>uninstall</a>");
	    $('#widget').append("<a href='javascript:back()' style='position:absolute;right:20px;bottom:10px'>back</a>");
		enableTab('widget');
		busy.hide();
	    if ($('#walletBtn').attr('class') == 'optbutton notselected') alert('Card was succesfully installed on your wallet.');
	} catch (e) {
		busy.hide();
		ShowErrorMessage(e);
	}
}

function back() {
	if ($('#installed').length) loadWalletContent();
	else loadMarketplace();
}

// Display items
function displayItems(items) {
	var ul = $('#itemsList'), i, item, li, text;
	var getCurrentItem = function (item, i) {
		return function() {
			displayDescriptionTab(item, i);
		};
	};
	// Remove previous items
	ul.empty();
	// Loop through the items
	for (i = 0; i < items.length; i += 1) {
		item = items[i];
		// Create new <li> element
		li = $('<li/>');
		// Create new <a> element
		text = $('<a/>', {'id' : 'a' + i, 'url' : item.link, 'style' : 'font-weight:bold;color:navy; text-decoration:underline'}).html(item.title);
		// Append the text element to the li element
		li.append(text);
		// Call display publication date function
		displayPubDate(item, li);	
		// Add an observer to the link, after the link was added to the DOM
		// the li is for the iPad optimization
		li.click(getCurrentItem(item, i));
		// Append the li element to the ul element
		ul.append(li);		
	}
	$('#inboxno').html(items.length);
	readMsgs = '';
	$('#feedsWrapper').show();
}

function displayPubDate(item, li) {
	var pubDate = $('<div/>', {'style' : 'font-size:11px; color:#aaa;padding-bottom: 10px'}).html(item.pubDate);
	li.append(pubDate);
}

function displayDescriptionTab(item, i) {
	$('#a' + i).css('font-weight', 'normal');
	if (readMsgs.indexOf(i) < 0) readMsgs += i;
	$('#inboxno').html((4 - readMsgs.length) == 0 ? "&nbsp;" : (4 - readMsgs.length));
	$('#description a').unbind();
	$('#description').html(item.description);
	$('#description a').click(function (event) {
		event.preventDefault();
		WL.App.openURL(event.target.href, '_blank');
	});
	$('#descriptionTab').show();
}

function enableTab(btn) {
	if (btn != 'widget') {
		$('#walletBtn').attr('class', 'optbutton ' + (btn == 'walletBtn' ? '' : 'not') + 'selected');
		$('#marketplaceBtn').attr('class', 'optbutton ' + (btn == 'marketplaceBtn' ? '' : 'not') + 'selected');
		$('#inboxBtn').attr('class', 'optbutton ' + (btn == 'inboxBtn' ? '' : 'not') + 'selected');
	}
	if (btn == 'widget') $('#widget').show(); else $('#widget').hide();
	if (btn == 'widget') $('#toolbar').hide(); else $('#toolbar').show();
	if (btn == 'walletBtn') $('#wallet').show(); else $('#wallet').hide(); 
	if (btn == 'marketplaceBtn') $('#marketplace').show(); else $('#marketplace').hide(); 
	if (btn == 'inboxBtn') $('#inbox').show(); else $('#inbox').hide();
	$('#AuthDiv').hide();
	$('#AppDiv').show();
}

function ShowErrorMessage(err) {
	var msg = err ? "Error processing data retrieved by the server:\n\n" + err : "Error retrieving data from server";
	WL.SimpleDialog.show('Worklight Application Error', msg, [{text : 'OK'}]);
}

// Cross platform openURL
function openURL(url, target) {
	// Cross platform open URL
	WL.Client.openURL(decodeURI(url), target);
}

function getThumbnailWidth() {
	return (0.85*$(window).width() - 96) / 2;
}

function loadData(procedure, successFunction, parameters) {
	busy.show();

	WL.Client.connect({
    	onSuccess: onConnectSuccess,
    	onFailure: onConnectFailure
    });
		
	var invocationData = {
		adapter: "MeaWalletRSS",
		procedure: procedure,
		parameters: []
	};
	if (parameters instanceof Array) invocationData.parameters = parameters;

	WL.Client.invokeProcedure(invocationData, {
		onSuccess : successFunction,
		onFailure : onGetFail,
		timeout : 30000
	});
}

function onGetFail(response) {	
	busy.hide();
	ShowErrorMessage();
}

function loadMarketplace() {
	loadData("getMarketplace", onLoadMarketplaceSuccess);
}

function loadWalletContent() {
	loadData("getWalletContent", onLoadWalletContentSuccess);
}

function openWidget(id) {
	loadData("getWidget", onOpenWidgetSuccess, [id]);
}

function deleteWidget(id) {
	if (confirm("About to uninstall this card. Are you sure?"))
		loadData("uninstallWidget", back, [id]);
}

function callWidgetBusiness(procedure, parameters) {
	loadData(procedure, onOpenWidgetSuccess, parameters);
}

function loadInbox() {
	loadData("getInbox", onLoadInboxSuccess);
}

function onConnectSuccess(){
    WL.Logger.debug("onConnectSuccess");
}

function onConnectFailure(){
	WL.Logger.debug("onConnectFailure");
	alert("Unable to connect to MeaWallet server. Please check your internet connection.");
}

function logout() {
	WL.Client.logout("AuthRealm");
	setTimeout(loadWalletContent, 1000);
}

function wlCommonInit(){
	busy = new WL.BusyIndicator();
	
	WL.Logger.info("App initialized");
	
	loadWalletContent();
}
