var authRealmChallengeHandler = WL.Client.createChallengeHandler("AuthRealm");

authRealmChallengeHandler.isCustomResponse = function(response) {
	if (!response || !response.responseJSON	|| response.responseText === null) {
		return false;
	}
	if (typeof(response.responseJSON.authRequired) !== 'undefined'){
		return true;
	} else {
		return false;
	}
};

authRealmChallengeHandler.handleChallenge = function(response){
	var authRequired = response.responseJSON.authRequired;

	if (authRequired == true){
		busy.hide();
		$("#AppDiv").hide();
		$("#AuthDiv").show();
		$("#password").val('');
		$("#AuthInfo").html('');
		$('#username').select();
		$('#username').focus();

		if (response.responseJSON.errorMessage)
	    	$("#AuthInfo").html(response.responseJSON.errorMessage);
		
	} else if (authRequired == false){
		$("#AppDiv").show();
		$("#AuthDiv").hide();
		authRealmChallengeHandler.submitSuccess();
	}
};


$("#AuthSubmitButton").bind('click', function () {
	var username = $("#username").val();
	var password = $("#password").val();

	WL.Client.connect({
    	onSuccess: onConnectSuccess,
    	onFailure: onConnectFailure
    });

	var invocationData = {
		adapter : "AuthenticationAdapter",
		procedure : "loginUser",
		parameters : [ username, password ]
	};

	authRealmChallengeHandler.submitAdapterAuthentication(invocationData, {});
});