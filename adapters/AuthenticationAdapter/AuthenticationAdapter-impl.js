/*
 *  Licensed Materials - Property of IBM
 *  5725-I43 (C) Copyright IBM Corp. 2011, 2013. All Rights Reserved.
 *  US Government Users Restricted Rights - Use, duplication or
 *  disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
 */

/**
 * WL.Server.invokeHttp(parameters) accepts the following json object as an
 * argument:
 *  { // Mandatory method : 'get' , 'post', 'delete' , 'put' or 'head' path:
 * value,
 *  // Optional returnedContentType: any known mime-type or one of "json",
 * "css", "csv", "javascript", "plain", "xml", "html" returnedContentEncoding :
 * 'encoding', parameters: {name1: value1, ... }, headers: {name1: value1, ... },
 * cookies: {name1: value1, ... }, body: { contentType: 'text/xml;
 * charset=utf-8' or similar value, content: stringValue }, transformation: {
 * type: 'default', or 'xslFile', xslFile: fileName } }
 */

function loginUser(username, password) {
	// call ESB :)
	if (username === "test" && password === "test" || username === "mea" && password === "mea") {

		var userIdentity = {
			userId : username,
			displayName : username,
			attributes : {
				CustomeAttribute1 : 'Some Data here',
				CustomeAttribute2 : 'Some more data here'
			}
		};

		WL.Server.setActiveUser("AuthRealm", null);
		WL.Server.setActiveUser("AuthRealm", userIdentity);

		return { authRequired : false };
	} else {
		var errorMsg = "Invalid login credentials";
		if (username.trim() === "") errorMsg = "Please enter username";
		else if (username !== "test" && username !== "mea") errorMsg = "Unknown user";
		else if (password.trim() === "") errorMsg = "Please enter password";
		
		return onAuthRequired(null, errorMsg);
	}
}

function onLogout() {
	WL.Server.setActiveUser("AuthRealm", null);
	WL.Logger.debug("Logged out");
}

function onAuthRequired(headers, errorMessage) {
	errorMessage = errorMessage ? errorMessage : null;

	return {
		authRequired : true,
		errorMessage : errorMessage
	};
}