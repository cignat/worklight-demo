/**
 *  WL.Server.invokeHttp(parameters) accepts the following json object as an argument:
 *  
 *  {
 *  	// Mandatory 
 *  	method : 'get' , 'post', 'delete' , 'put' or 'head' 
 *  	path: value,
 *  	
 *  	// Optional 
 *  	returnedContentType: any known mime-type or one of "json", "css", "csv", "javascript", "plain", "xml", "html"  
 *  	returnedContentEncoding : 'encoding', 
 *  	parameters: {name1: value1, ... }, 
 *  	headers: {name1: value1, ... }, 
 *  	cookies: {name1: value1, ... }, 
 *  	body: { 
 *  		contentType: 'text/xml; charset=utf-8' or similar value, 
 *  		content: stringValue 
 *  	}, 
 *  	transformation: { 
 *  		type: 'default', or 'xslFile', 
 *  		xslFile: fileName 
 *  	} 
 *  } 
 */

var pushNotifications = new Array();

WL.Server.createEventSource({
	name: "pollBackendNotifications",
	poll: {
		interval: 30,
		onPoll: "pollBackendNotifications",
	}
});

/**
 * 
 * @param
 * @returns
 */
function deployServiceInstance(serviceId, userId, authCode, seId) {
	var input = {
			method : 'get',
			returnedContentType : 'json',
			parameters : {
				'serviceId' : serviceId,
				'serviceInstanceId' : 1,
				'userId' : userId,
				'authCode' : authCode,
				'seId' : seId
			},
			path : WL.Server.configuration["esb.baseurl"] + WL.Server.configuration["esb.method.service.deploy"]
		};
	
	return WL.Server.invokeHttp(input);
}

/**
 * 
 * @param
 * @returns
 */
function terminateServiceInstance(serviceInstanceId) {
	var input = {
			method : 'get',
			returnedContentType : 'json',
			parameters : {
				'serviceInstanceId' : serviceInstanceId
			},
			path : WL.Server.configuration["esb.baseurl"] + WL.Server.configuration["esb.method.service.terminate"]
		};

	return WL.Server.invokeHttp(input);
}

/**
 * 
 * @param
 * @returns
 */
function getNotifications() {
	//mark push notifications as processed
	var input = {
			method : 'get',
			returnedContentType : 'json',
			parameters: pushNotifications,
			path : WL.Server.configuration["esb.baseurl"] + WL.Server.configuration["esb.method.notification.update"]
		};
	WL.Server.invokeHttp(input);
	return notifications;
}

/**
 * 
 * @param
 * @returns
 */
function pollBackendNotifications() {
	var input = {
			method : 'get',
			returnedContentType : 'json',
			path : WL.Server.configuration["esb.baseurl"] + WL.Server.configuration["esb.method.notification.get"]
		};

	var backendNotifications = WL.Server.invokeHttp(input);
	processBackendNotifications(backendNotifications);
}

/**
 * 
 * @param
 * @returns
 */
function processBackendNotifications(notifications) {
	if (notifications == undefined || !(notifications instanceof Array) || notifications.length <= 0) return;
	
	var len = notifications.length;
	for (var i = 0; i < len; i++) {
		var notification = notifications[i];
		//if backend notification, process and update status on esb
		if (false) {
			
		} else {
			pushNotifications.push(notification);
		}
	}
}